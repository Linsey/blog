---
title: Workshop blogging
date: 2017-10-06
---
# Eerste post

Dit is de eerste post voor de **workshop**.
Zoek op wikipedia markdown voor alle markdowns.

## Done
Bij deze workshop hebben we gedaan:
1. Gitlab aangemaakt.
2. Hugo geforkt.
3. Settings aangepast.
4. Blogposts verwijderd.
5. Deze post maken en online zetten. 

## Verder
Wat nog gedaan moet worden:
* Thema wijzigen
* Meer settings aanpassen
* Meer _bloggen_
